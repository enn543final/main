import numpy as np
import pandas as pd
#import pickle
#from keras.models import Sequential
#from keras.layers import Dense, Activation, Dropout
import scipy.sparse
from sklearn import decomposition
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split
from sklearn.externals import joblib
from sklearn.pipeline import Pipeline
from sklearn.grid_search import GridSearchCV

'''This Script Performs regression on the binary attributes of the dataset'''
#For loading in our data
train_test_path= '../CUB_200_2011/train_test_split.txt'
attributes_path= '../CUB_200_2011/attributes/image_attribute_labels.txt';
image_path     = '../CUB_200_2011/image_class_labels.txt';
#To get in the format I wanted
data = np.genfromtxt(attributes_path, dtype='int32', usecols =(0,1,2))
data = pd.DataFrame(data)
data = data.pivot(index = 0, columns = 1, values = 2)
data.reset_index(level=0,inplace=True)
image_data = np.genfromtxt(image_path, dtype='int32', usecols = (1))
image_data = pd.DataFrame(image_data)
#Split data into training vs test
train_data = pd.DataFrame()
test_data = pd.DataFrame()
train_y = pd.DataFrame()
test_y = pd.DataFrame()
# 1 is training_image
train_test_split_index = pd.read_csv(train_test_path, delimiter=' ', header=None)
#j = 0
#for ii in train_test_split_index[1]:
#    if ii == 1:
#        train_data=train_data.append(data.loc[j])
#        train_y = train_y.append(image_data.loc[j])
#    else:
#        test_data=test_data.append(data.loc[j])
#        test_y = test_y.append(image_data.loc[j])
#    j+=1

#FOR TEXT FILE TRAIN VS TEST - HAVE ONLY ONE ON
#train_x = train_data.drop(0, axis = 1)
#test_x = test_data.drop(0, axis = 1)
#X_train = train_x.values
#X_test = test_x.values
#y_train = np.ravel(train_y.values)
#y_test = np.ravel(test_y.values)

#FOR RANDOM TRAIN VS TEST SPLIT - HAVE ONLY ONE ON
X = data.drop(0, axis=1)
y = image_data
X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.3, random_state=0)
X_train = X_train.values
X_test = X_test.values
y_train = np.ravel(y_train)
y_test = np.ravel(y_test)

clf = LogisticRegression(C=0.01, random_state=0, multi_class='multinomial', solver = 'lbfgs')
pca = decomposition.PCA()
pipe = Pipeline(steps=[('pca', pca), ('logistic', clf)])

# Plot the PCA spectrum
pca.fit(X_train)

plt.figure(1, figsize=(4, 3))
plt.clf()
plt.axes([.2, .2, .7, .7])
plt.plot(pca.explained_variance_, linewidth=2)
plt.axis('tight')
plt.xlabel('n_components')
plt.ylabel('explained_variance_')

###############################################################################
# Prediction

n_components = [200]
Cs = [0.158]

#Parameters of pipelines can be set using ‘__’ separated parameter names:

estimator = GridSearchCV(pipe,
                         dict(pca__n_components=n_components,
                              logistic__C=Cs))
estimator.fit(X_train, y_train)

plt.axvline(estimator.best_estimator_.named_steps['pca'].n_components,
            linestyle=':', label='n_components chosen')
plt.legend(prop=dict(size=12))
plt.show()
print(estimator.score(X_test,y_test))
print(estimator.best_estimator_.named_steps['logistic'].C)