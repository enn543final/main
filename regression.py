import numpy as np
import pandas as pd
#import pickle
#from keras.models import Sequential
#from keras.layers import Dense, Activation, Dropout
import scipy.sparse
#import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split
from sklearn.externals import joblib
'''This Script Performs regression on the binary attributes of the dataset'''
#For loading in our data
train_test_path= '../CUB_200_2011/train_test_split.txt'
attributes_path= '../CUB_200_2011/attributes/image_attribute_labels.txt';
image_path     = '../CUB_200_2011/image_class_labels.txt';
#To get in the format I wanted
data = np.genfromtxt(attributes_path, dtype='int32', usecols =(0,1,2))
data = pd.DataFrame(data)
data = data.pivot(index = 0, columns = 1, values = 2)
data.reset_index(level=0,inplace=True)
image_data = np.genfromtxt(image_path, dtype='int32', usecols = (1))
image_data = pd.DataFrame(image_data)
#Split data into training vs test
train_data = pd.DataFrame()
test_data = pd.DataFrame()
train_y = pd.DataFrame()
test_y = pd.DataFrame()
# 1 is training_image
train_test_split_index = pd.read_csv(train_test_path, delimiter=' ', header=None)
j = 0
for ii in train_test_split_index[1]:
    if ii == 1:
        train_data=train_data.append(data.loc[j])
        train_y = train_y.append(image_data.loc[j])
    else:
        test_data=test_data.append(data.loc[j])
        test_y = test_y.append(image_data.loc[j])
    j+=1

#Lastly Split into X and Y train and test
train_x = train_data.drop(0, axis = 1)
test_x = test_data.drop(0, axis = 1)

#X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.15, random_state=0)

#Turn back to np
#X_train = X_train.values
#X_test = X_test.values
#y_train = np.ravel(y_train)
#y_test = np.ravel(y_test)

X_train = train_x.values
X_test = test_x.values
y_train = np.ravel(train_y.values)
y_test = np.ravel(test_y.values)


print("Training SVM model")
clf = svm.SVC(decision_function_shape='ovo', verbose=False)
clf.fit(X_train,y_train)

#dec = clf.decision_function([[1]])
#print(dec.shape[1])
#save model
joblib.dump(clf, 'SVM.joblib')
clf2 = joblib.load('SVM.joblib')

print("Training Random Forest model")
clfRFC=RandomForestClassifier(n_estimators=5,
                              criterion = 'entropy',
                              random_state = 42, verbose=False)
clfRFC.fit(X_train,y_train)
joblib.dump(clfRFC, 'RFC.joblib')
clfRFC = joblib.load('RFC.joblib')

print("Training Naive Bayes Model")
clfNB = GaussianNB()
clfNB.fit(X_train,y_train)
joblib.dump(clfNB,'NB.joblib')
clfNB = joblib.load('NB.joblib')

print("Linear")
clflin= svm.LinearSVC(random_state=0, tol=1e-5,
                      multi_class = 'crammer_singer')
clflin.fit(X_train,y_train)

y_pred1 = clf2.predict(X_test)
y_pred2 = clfRFC.predict(X_test)
y_pred3 = clfNB.predict(X_test)
y_pred4 = clflin.predict(X_test)
print("First 10 Predictions")
print("SVM:", y_pred1[1:10])
print("RF:", y_pred2[1:10])
print("NB:", y_pred3[1:10])
print("LIN", y_pred4[1:10])
print("Actual:", y_test[1:10])
SVM = 0
RF = 0
NB= 0
GPC = 0
for i in range(5793):
    if y_pred1[i] == y_test[i]:
        SVM+=1
    if y_pred2[i] == y_test[i]:
        RF +=1
    if y_pred3[i] == y_test[i]:
        NB +=1
    if y_pred4[i] == y_test[i]:
        GPC +=1
print("Accuracy of SVM:",SVM/len(y_test))
print("Accuracy of RF:", RF/len(y_test))
print("Accuracy of NB:", NB/len(y_test))
print("Accuracy of LIN:", GPC/len(y_test))
#revfactor = dict(zip(range(200)))
#y_test = np.vectorize(revfactor.get)(y_test)
#y_pred = np.vectorize(revfactor.get)(y_pred)
#print(pd.crosstab(y_test,y_pred))