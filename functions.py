from PIL import Image
from scipy import ndimage
import matplotlib.pyplot as plt
import os
import numpy as np
import pandas as pd
import string
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten

import sklearn
from sklearn import cross_validation, grid_search
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.svm import SVC
from sklearn.externals import joblib


def data_split(train_test_split, attributes, attribute_labels, image_class_labels):
    #Create a new array for X data only using the confidence variable for now
    #Reshape data into Birds x Attributes
    new_attribute = attributes.pivot(index = 0, columns = 1, values = 3)
    new_attribute.columns = attribute_labels[1]
    new_attribute.fillna(0)
    #Define which photos are for training and which are for testing
    train_image = train_test_split.loc[train_test_split[1] == 1]
    test_image = train_test_split.loc[train_test_split[1] == 0]
    
        
    #Find the index of the variables
    index = []
    for ii in train_image[0]:
        index.append(ii)    
    X_train = new_attribute.loc[index]
    Y_train = image_class_labels.loc[index,1]
    index = []
    for ii in test_image[0]:
        index.append(ii)    
    X_test = new_attribute.loc[index]
    Y_test = image_class_labels.loc[index,1]
    #Drop off the shitty indexs
    Y_train=Y_train.reset_index(drop=True)
    Y_test=Y_test.reset_index(drop=True)
    X_train=X_train.reset_index(drop=True)
    X_test=X_test.reset_index(drop=True)
    #Y_test.set_value(5793,1,200)
    return Y_train,X_train,Y_test,X_test
                
        
    
    
    
def importimages(path):
    imagelib = {}
    dircontents = os.listdir(path)
    for ii in dircontents:
        imagelist = os.listdir(path+ii)
        species = {}
        #print(imagelist)
        for jj in imagelist: 
            temp = Image.open(path+ii+"/"+jj)
            species[jj] = temp.copy()           #Need to copy over the image 
            temp.close()
            #plt.imshow(species[jj])
            #plt.show()
            #print(jj)
        #print(species)
        imagelib[ii] = species
        print(ii)
        if ii == '020.Yellow_breasted_Chat':
            break
    return imagelib

def import_text_files(file):
    path = '../CUB_200_2011/'
    f = pd.read_csv(os.path.join(path,file), header=None,sep='\s+', error_bad_lines=False)
    return f 

def class_attribute_label(file):
    path = '../CUB_200_2011/'
    f = pd.read_csv(os.path.join(path,file), header =None, sep='\s+')
    return f

def displayimage(imagelib, species, number):
    # Get bird image from dictionary
    species = list(imagelib.keys())[species]
    birddict = imagelib.get(species)
    birdname = list(birddict.keys())[number]
    birdimage = birddict.get(birdname)
    print(birdimage)
    plt.imshow(birdimage)
    plt.title("{}".format(species))
    plt.show()
    


def processImages():
    print("Process Images")


'''Example from net. Not for use yet'''
def createModel():
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=input_shape))
    model.add(Conv2D(32, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
 
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
 
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
 
    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(nClasses, activation='softmax'))
     
    return model





