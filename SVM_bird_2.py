import sys
import os
import itertools
import random
from PIL import Image  # PIL
from svmutil import *  # libSVM
import numpy as np

# Image data constants
DIMENSION = 32
ROOT_DIR = "../CUB_200_2011/images/"
A = "001.Black_footed_Albatross"
B = "002.Laysan_Albatross"
C = "003.Sooty_Albatross"
D = "004.Groove_billed_Ani"
E = "005.Crested_Auklet"
F = "006.Least_Auklet"
G = "007.Parakeet_Auklet"
H = "008.Rhinoceros_Auklet"
I = "009.Brewer_Blackbird"
J = "010.Red_winged_Blackbird"
K = "011.Rusty_Blackbird"
L = "012.Yellow_headed_Blackbird"
M = "013.Bobolink"
N = "014.Indigo_Bunting"
O = "015.Lazuli_Bunting"
P = "016.Painted_Bunting"
Q = "017.Cardinal"
R = "018.Spotted_Catbird"
S = "019.Gray_Catbird"
T = "020.Yellow_breasted_Chat"
U = "021.Eastern_Towhee"
V = "022.Chuck_will_Widow"
W = "023.Brandt_Cormorant"
X = "024.Red_faced_Cormorant"
Y = "025.Pelagic_Cormorant"
Z = "026.Bronzed_Cowbird"
A1 = "027.Shiny_Cowbird"
B1 = "028.Brown_Creeper"
C1 = "029.American_Crow"
D1 = "030.Fish_Crow"
E1 = "031.Black_billed_Cuckoo"
F1 = "032.Mangrove_Cuckoo"
G1 = "033.Yellow_billed_Cuckoo"
H1 = "034.Gray_crowned_Rosy_Finch"
I1 = "035.Purple_Finch"
J1 = "036.Northern_Flicker"
K1 = "037.Acadian_Flycatcher"
L1 = "038.Great_Crested_Flycatcher"
M1 = "039.Least_Flycatcher"
N1 = "040.Olive_sided_Flycatcher"
O1 = "041.Scissor_tailed_Flycatcher"
P1 = "042.Vermilion_Flycatcher"
Q1 = "043.Yellow_bellied_Flycatcher"
R1 = "044.Frigatebird"
S1 = "045.Northern_Fulmar"
T1 = "046.Gadwall"
U1 = "047.American_Goldfinch"
V1 = "048.European_Goldfinch"
W1 = "049.Boat_tailed_Grackle"
X1 = "050.Eared_Grebe"
Y1 = "051.Horned_Grebe"
Z1 = "052.Pied_billed_Grebe"
A2 = "053.Western_Grebe"
B2 = "054.Blue_Grosbeak"
C2 = "055.Evening_Grosbeak"
D2 = "056.Pine_Grosbeak"
E2 = "057.Rose_breasted_Grosbeak"
F2 = "058.Pigeon_Guillemot"
G2 = "059.California_Gull"
H2 = "060.Glaucous_winged_Gull"
I2 = "061.Heermann_Gull"
J2 = "062.Herring_Gull"
K2 = "063.Ivory_Gull"
L2 = "064.Ring_billed_Gull"
M2 = "065.Slaty_backed_Gull"
N2 = "066.Western_Gull"
O2 = "067.Anna_Hummingbird"
P2 = "068.Ruby_throated_Hummingbird"
Q2 = "069.Rufous_Hummingbird"
R2 = "070.Green_Violetear"
S2 = "071.Long_tailed_Jaeger"
T2 = "072.Pomarine_Jaeger"
U2 = "073.Blue_Jay"
V2 = "074.Florida_Jay"
W2 = "075.Green_Jay"
X2 = "076.Dark_eyed_Junco"
Y2 = "077.Tropical_Kingbird"
Z2 = "078.Gray_Kingbird"
A3 = "079.Belted_Kingfisher"
B3 = "080.Green_Kingfisher"
C3 = "081.Pied_Kingfisher"
D3 = "082.Ringed_Kingfisher"
E3 = "083.White_breasted_Kingfisher"
F3 = "084.Red_legged_Kittiwake"
G3 = "085.Horned_Lark"
H3 = "086.Pacific_Loon"
I3 = "087.Mallard"
J3 = "088.Western_Meadowlark"
K3 = "089.Hooded_Merganser"
L3 = "090.Red_breasted_Merganser"
M3 = "091.Mockingbird"
N3 = "092.Nighthawk"
O3 = "093.Clark_Nutcracker"
P3 = "094.White_breasted_Nuthatch"
Q3 = "095.Baltimore_Oriole"
R3 = "096.Hooded_Oriole"
S3 = "097.Orchard_Oriole"
T3 = "098.Scott_Oriole"
U3 = "099.Ovenbird"
V3 = "100.Brown_Pelican"
W3 = "101.White_Pelican"
X3 = "102.Western_Wood_Pewee"
Y3 = "103.Sayornis"
Z3 = "104.American_Pipit"
A4 = "105.Whip_poor_Will"
B4 = "106.Horned_Puffin"
C4 = "107.Common_Raven"
D4 = "108.White_necked_Raven"
E4 = "109.American_Redstart"
F4 = "110.Geococcyx"
G4 = "111.Loggerhead_Shrike"
H4 = "112.Great_Grey_Shrike"
I4 = "113.Baird_Sparrow"
J4 = "114.Black_throated_Sparrow"
K4 = "115.Brewer_Sparrow"
L4 = "116.Chipping_Sparrow"
M4 = "117.Clay_colored_Sparrow"
N4 = "118.House_Sparrow"
O4 = "119.Field_Sparrow"
P4 = "120.Fox_Sparrow"
Q4 = "121.Grasshopper_Sparrow"
R4 = "122.Harris_Sparrow"
S4 = "123.Henslow_Sparrow"
T4 = "124.Le_Conte_Sparrow"
U4 = "125.Lincoln_Sparrow"
V4 = "126.Nelson_Sharp_tailed_Sparrow"
W4 = "127.Savannah_Sparrow"
X4 = "128.Seaside_Sparrow"
Y4 = "129.Song_Sparrow"
Z4 = "130.Tree_Sparrow"
A5 = "131.Vesper_Sparrow"
B5 = "132.White_crowned_Sparrow"
C5 = "133.White_throated_Sparrow"
D5 = "134.Cape_Glossy_Starling"
E5 = "135.Bank_Swallow"
F5 = "136.Barn_Swallow"
G5 = "137.Cliff_Swallow"
H5 = "138.Tree_Swallow"
I5 = "139.Scarlet_Tanager"
J5 = "140.Summer_Tanager"
K5 = "141.Artic_Tern"
L5 = "142.Black_Tern"
M5 = "143.Caspian_Tern"
N5 = "144.Common_Tern"
O5 = "145.Elegant_Tern"
P5 = "146.Forsters_Tern"
Q5 = "147.Least_Tern"
R5 = "148.Green_tailed_Towhee"
S5 = "149.Brown_Thrasher"
T5 = "150.Sage_Thrasher"
U5 = "151.Black_capped_Vireo"
V5 = "152.Blue_headed_Vireo"
W5 = "153.Philadelphia_Vireo"
X5 = "154.Red_eyed_Vireo"
Y5 = "155.Warbling_Vireo"
Z5 = "156.White_eyed_Vireo"
A6 = "157.Yellow_throated_Vireo"
B6 = "158.Bay_breasted_Warbler"
C6 = "159.Black_and_white_Warbler"
D6 = "160.Black_throated_Blue_Warbler"
E6 = "161.Blue_winged_Warbler"
F6 = "162.Canada_Warbler"
G6 = "163.Cape_May_Warbler"
H6 = "164.Cerulean_Warbler"
I6 = "165.Chestnut_sided_Warbler"
J6 = "166.Golden_winged_Warbler"
K6 = "167.Hooded_Warbler"
L6 = "168.Kentucky_Warbler"
M6 = "169.Magnolia_Warbler"
N6 = "170.Mourning_Warbler"
O6 = "171.Myrtle_Warbler"
P6 = "172.Nashville_Warbler"
Q6 = "173.Orange_crowned_Warbler"
R6 = "174.Palm_Warbler"
S6 = "175.Pine_Warbler"
T6 = "176.Prairie_Warbler"
U6 = "177.Prothonotary_Warbler"
V6 = "178.Swainson_Warbler"
W6 = "179.Tennessee_Warbler"
X6 = "180.Wilson_Warbler"
Y6 = "181.Worm_eating_Warbler"
Z6 = "182.Yellow_Warbler"
A7 = "183.Northern_Waterthrush"
B7 = "184.Louisiana_Waterthrush"
C7 = "185.Bohemian_Waxwing"
D7 = "186.Cedar_Waxwing"
E7 = "187.American_Three_toed_Woodpecker"
F7 = "188.Pileated_Woodpecker"
G7 = "189.Red_bellied_Woodpecker"
H7 = "190.Red_cockaded_Woodpecker"
I7 = "191.Red_headed_Woodpecker"
J7 = "192.Downy_Woodpecker"
K7 = "193.Bewick_Wren"
L7 = "194.Cactus_Wren"
M7 = "195.Carolina_Wren"
N7 = "196.House_Wren"
O7 = "197.Marsh_Wren"
P7 = "198.Rock_Wren"
Q7 = "199.Winter_Wren"
R7 = "200.Common_Yellowthroat"



CLASSES = [A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, A1, B1, C1, D1, E1, F1, G1, H1, I1, J1, K1, L1, M1, N1, O1, P1, Q1, R1, S1, T1, U1, V1, W1, X1, Y1, Z1, A2, B2, C2, D2, E2, F2, G2, H2, I2, J2, K2, L2, M2, N2, O2, P2, Q2, R2, S2, T2, U2, V2, W2, X2, Y2, Z2, A3, B3, C3, D3, E3, F3, G3, H3, I3, J3, K3, L3, M3, N3, O3, P3, Q3, R3, S3, T3, U3, V3, W3, X3, Y3, Z3, A4, B4, C4, D4, E4, F4, G4, H4, I4, J4, K4, L4, M4, N4, O4, P4, Q4, R4, S4, T4, U4, V4, W4, X4, Y4, Z4, A5, B5, C5, D5, E5, F5, G5, H5, I5, J5, K5, L5, M5, N5, O5, P5, Q5, R5, S5, T5, U5, V5, W5, X5, Y5, Z5, A6, B6, C6, D6, E6, F6, G6, H6, I6, J6, K6, L6, M6, N6, O6, P6, Q6, R6, S6, T6, U6, V6, W6, X6, Y6, Z6, A7, B7, C7, D7, E7, F7, G7, H7, I7, J7, K7, L7, M7, N7, O7, P7, Q7, R7]

# libsvm constants
LINEAR = 0
RBF = 2

# Other
USE_LINEAR = False
IS_TUNING = False

def main():
    train, tune, test = getData(IS_TUNING)
    models = getModels(train)
    try:
        train, tune, test = getData(IS_TUNING)
        models = getModels(train)
        results = None
        if IS_TUNING:
            print ("!!! TUNING MODE !!!")
            results = classify(models, tune)
        else:
            results = classify(models, test)
            
        totalCount = 0
        totalCorrect = 0
        for clazz in CLASSES:
            count, correct = results[clazz]
            totalCount += count
            totalCorrect += correct
            print ("%s %d %d %f" % (clazz, correct, count, (float(correct) / count)))
        print ("%s %d %d %f" % ("Overall", totalCorrect, totalCount, (float(totalCorrect) / totalCount)))

    except Exception as e:
        print (e)
        return 5

def classify(models, dataSet):
    results = {}
    for trueClazz in CLASSES:
        count = 0
        correct = 0
        for item in dataSet[trueClazz]:
            predClazz, prob = predict(models, item)
            print ("%s,%s,%f" % (trueClazz, predClazz, prob))
            count += 1
            if trueClazz == predClazz: correct += 1
        results[trueClazz] = (count, correct)
    return results

def predict(models, item):
    maxProb = 0.0
    bestClass = ""
    for clazz, model in models.iteritems():
        prob = predictSingle(model, item)
        if prob > maxProb:
            maxProb = prob
            bestClass = clazz
    return (bestClass, maxProb)

def predictSingle(model, item):
    output = svm_predict([0], [item], model, "-q -b 1")
    prob = output[2][0][0]
    return prob

def getModels(trainingData):
    models = {}
    param = getParam(USE_LINEAR)
    for c in CLASSES:
        labels, data = getTrainingData(trainingData, c)
        print(type(labels), type(data))
        prob = svm_problem(labels, data)
        m = svm_train(prob, param)
        models[c] = m
    return models

def getTrainingData(trainingData, clazz):
    labeledData = getLabeledDataVector(trainingData, clazz, 1)
    negClasses = [c for c in CLASSES if not c == clazz]
    for c in negClasses:
        ld = getLabeledDataVector(trainingData, c, -1)
        labeledData += ld
    random.shuffle(labeledData)
    unzipped = [list(t) for t in zip(*labeledData)]
    labels, data = unzipped[0], unzipped[1]
    return (labels, data)

def getParam(linear = True):
    param = svm_parameter("-q")
    param.probability = 1
    if(linear):
        param.kernel_type = LINEAR
        param.C = .01
    else:
        param.kernel_type = RBF
        param.C = .01
        param.gamma = .00000001
    return param

def getLabeledDataVector(dataset, clazz, label):
    data = dataset[clazz]
    labels = [label] * len(data)
    output = zip(labels, data)
    return output

def getData(generateTuningData):
    trainingData = {}
    tuneData = {}
    testData = {}
    
    for clazz in CLASSES:
        (train, tune, test) = buildTrainTestVectors(buildImageList(ROOT_DIR + clazz + "/"), generateTuningData)
        trainingData[clazz] = train
        tuneData[clazz] = tune
        testData[clazz] = test
    
    return (trainingData, tuneData, testData)

def buildImageList(dirName):
    imgs = [Image.open(dirName + fileName).resize((DIMENSION, DIMENSION)) for fileName in os.listdir(dirName)]
    #print(imgs)
    imgs2 = []
    for img in imgs:
        print(list(img.getdata()))
        imgs2.append(list(itertools.chain.from_iterable(img.getdata())))
        
    return imgs2

def buildTrainTestVectors(imgs, generateTuningData):
    # 70% for training, 30% for test.
    testSplit = int(.7 * len(imgs))
    baseTraining = imgs[:testSplit]
    test = imgs[testSplit:]

    training = None
    tuning = None
    if generateTuningData:
        # 50% of training for true training, 50% for tuning.
        tuneSplit = int(.5 * len(baseTraining))
        training = baseTraining[:tuneSplit]
        tuning = baseTraining[tuneSplit:]
    else:
        training = baseTraining

    return (training, tuning, test)

if __name__ == "__main__":
    sys.exit(main())
