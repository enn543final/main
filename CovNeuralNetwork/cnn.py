import tensorflow as tf
from keras.models import Sequential, Model
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from matplotlib import pyplot as plt
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
import numpy as np
from keras import backend as K
import json, codecs
from keras.applications import resnet50, vgg16
from keras.callbacks import ReduceLROnPlateau, EarlyStopping

def main():
    
        #Data directories
        train_data_dir = "CUB_200_2011/train_images/"
        test_data_dir = "test_data/"
        validation_data_dir = "CUB_200_2011/val_images/"
        K.clear_session()
        #create model function
        #createmodel(train_data_dir, test_data_dir, validation_data_dir)
        usepretrainedmodel(train_data_dir, test_data_dir, validation_data_dir)
    
    
        #verbose=-2)
def usepretrainedmodel(train_data_dir, test_data_dir, validation_data_dir):
        ######Definitions
        #Image size for CNN
        img_width, img_height = 150, 150
        #Number of training samples
        nb_train_samples = 8244
        #number of validation samples
        nb_validation_samples = 3544
        epochs = 100
        batch_size = 5
        
        # reshape image for CNN
        if K.image_data_format() == 'channels_first':
            input_shape = (3, img_width, img_height)
        else:
            input_shape = (img_width, img_height, 3)
        
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.9)
        config = tf.ConfigProto(gpu_options=gpu_options)
        config.gpu_options.allow_growth = True
       
        session = tf.Session(config=config)
        K.set_session(session)   
        
        ############## VGG16 ################
        
        #Configure pretrained model
        vgg16_model = vgg16.VGG16(weights="imagenet", include_top=False, input_shape=input_shape)
        model = Sequential()
        
        #Making all the layers without the output layer to be not trainable
        for layer in vgg16_model.layers[:-4]:
            layer.trainable = False 

        for layer in vgg16_model.layers:
            print(layer)

        model.add(vgg16_model)
        #Adding last layer for 
        
        model.add(Flatten())
        model.add(Dense(256, activation="relu"))
        model.add(Dropout(0.5))
        model.add(Dense(200, activation='softmax'))
        
        sgd = optimizers.SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True)


        model.compile(loss='categorical_crossentropy',
                      optimizer=sgd,
                      metrics=['accuracy'])
        
        model.summary()
        
        train_datagen = ImageDataGenerator(
             rotation_range=30,
             width_shift_range=0.2,
             height_shift_range=0.2,
             rescale=1./255,
             shear_range=0.2,
             zoom_range=0.2,
             horizontal_flip=True,
             fill_mode='nearest')
        
        # this is the augmentation configuration we will use for testing:
        # only rescaling
        test_datagen = ImageDataGenerator(rescale=1. / 255)
        
        train_generator = train_datagen.flow_from_directory(
            train_data_dir,
            target_size=(img_width, img_height),
            batch_size=batch_size)
        
        validation_generator = test_datagen.flow_from_directory(
            validation_data_dir,
            target_size=(img_width, img_height),
            batch_size=batch_size)
       
        reduce_lr=ReduceLROnPlateau(monitor="val_loss", factor=0.1, patience=4, min_lr=0.00001)

        early_stop= EarlyStopping(monitor="val_loss", min_delta=0, patience=5, verbose=0, mode="auto")

        callback_list = [reduce_lr, early_stop]

        hist = model.fit_generator(
                train_generator,
                steps_per_epoch = nb_train_samples // batch_size,
                epochs=epochs,
                shuffle=True,
                validation_steps=nb_validation_samples // batch_size,
                validation_data=validation_generator,
                callbacks=callback_list)
        
        length = len(hist.history['acc'])
        epochplot = list(np.linspace(1, length, length))

        #print(hist.history['acc'])

        hist.history['epochs'] = epochplot

        #print(hist.history)
        with open("results-"+str(epochs)+"-vgg16-350(preso).json", "w") as outfile:
            outfile.write("{")
            for key in hist.history:
                outfile.write(key+":"+str(hist.history[key])+",\n")
            outfile.write("}")
            outfile.close()
        
        model.save("VGG16-"+str(epochs)+"-350(preso).h5")
        
        plt.plot(epochplot, hist.history['acc'], color='blue')
        plt.plot(epochplot, hist.history['val_acc'], color='red')
        plt.show()
        
    
def createmodel(train_data_dir, test_data_dir, validation_data_dir):
        ######Definitions
        #Image size for CNN
        img_width, img_height = 150, 150
        #Number of training samples
        nb_train_samples = 38844
        #number of validation samples
        nb_validation_samples = 9718
        epochs = 80
        batch_size = 20
        
        # reshape image for CNN
        if K.image_data_format() == 'channels_first':
            input_shape = (3, img_width, img_height)
        else:
            input_shape = (img_width, img_height, 3)
        
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.7)
        config = tf.ConfigProto(gpu_options=gpu_options)
        config.gpu_options.allow_growth = True
       
        session = tf.Session(config=config)
        K.set_session(session)       
        
        #Define model for CNN
        #Custom Model
        model = Sequential()
        
        ## Adding 1st convolutional layer
        model.add(Conv2D(256, kernel_size=(3,3), input_shape=input_shape))
        model.add(Activation('relu'))
        model.add(Dropout(0.5))
        model.add(MaxPooling2D(2,2))
        
        #Add 2nd conv layer
        model.add(Conv2D(128, kernel_size=(3,3)))
        model.add(Activation('relu'))
        model.add(Dropout(0.5))
        model.add(MaxPooling2D(2,2))
        
        model.add(Conv2D(64, kernel_size=(3,3)))
        model.add(Activation('relu'))
        model.add(Dropout(0.5))
        model.add(MaxPooling2D(2,2))
        
        model.add(Flatten())
        model.add(Dense(200, activation='softmax'))
        
        model.compile(loss='categorical_crossentropy',
                      optimizer=optimizers.Adam(lr=0.0001),
                      metrics=['accuracy'])

        train_datagen = ImageDataGenerator(
             rotation_range=20,
             width_shift_range=0.1,
             height_shift_range=0.1,
             rescale=1./255,
             shear_range=0.1,
             zoom_range=0.1,
             horizontal_flip=True,
             fill_mode='nearest')

        train_generator = train_datagen.flow_from_directory(
            train_data_dir,
            target_size=(img_width, img_height),
            batch_size=batch_size)

        
        # this is the augmentation configuration we will use for testing:
        # only rescaling
        val_datagen = ImageDataGenerator(rescale=1. / 255)
        validation_generator = val_datagen.flow_from_directory(
            validation_data_dir,
            target_size=(img_width, img_height),
            batch_size=batch_size)
        
        # Run model
        hist = model.fit_generator(
                train_generator,
                steps_per_epoch = nb_train_samples // batch_size,
                epochs=epochs,
                validation_steps=nb_validation_samples // batch_size,
                validation_data=validation_generator)
        
        epochplot = list(np.linspace(1, epochs, epochs))

        hist.history['epochs'] = epochplot
        with open("results-"+str(epochs)+"-4.json", "w") as outfile:
            json.dump(hist.history, outfile)
    
        model.save("3_Layered_Conv_Network-"+str(epochs)+"-4.h5")

        plt.plot(epochplot, hist.history['acc'], color='blue')
        plt.plot(epochplot, hist.history['val_acc'], color='red')
        plt.show()


main()

