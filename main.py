from functions import *
import win32file
'''
if imagelib:
    pass
else:
    imagelib = {}
    imagepath = "../CUB_200_2011/images/"
    print('Loading in Images... This may take a while!')
    imagelib = importimages(imagepath)
'''
''' For General House Keeping
    Importing Text Files into relevant DataFrames
    '''
flag = 1
if flag == 1:
    #Image Reference
    images_ref = import_text_files('images.txt');
    #Suggested Training vs Test Split
    train_test_split = import_text_files('train_test_split.txt')
    #What Image is which Bird
    image_class_labels = import_text_files('image_class_labels.txt')
    #Class to Name of Bird
    classes = import_text_files('classes.txt')
    #Bounding Boxes
    bounding_boxes = import_text_files('bounding_boxes.txt')
    
    '''For Regression Techniques Using Attributes Rather than Images'''
    #Rating of Prediction
    certainty_label = import_text_files('attributes/certainties.txt')
    #Name of Attribute
    attribute_labels = import_text_files('attributes/attributes.txt')
    #Attributes (Note that Bird 2275 is dodgey)
    attributes = import_text_files('attributes/image_attribute_labels.txt')

    attribute_human =class_attribute_label('attributes/class_attribute_labels_continuous.txt')
'''Do Shit Here'''
[Y_train,X_train,Y_test,X_test]=data_split(train_test_split, 
                            attributes, attribute_labels, image_class_labels)

#Need to change datatype to np array
X_train = X_train.values
Y_train = Y_train.values
#Remove any NaN values
X_train[np.isnan(X_train)]=1
Y_train[np.isnan(Y_train)]=201
X_test[np.isnan(X_test)]=1
Y_test[np.isnan(Y_test)]=201

#Code from the internet Most definately not linear...
from sklearn.svm import SVC  
svclassifier = SVC(kernel='rbf')  
svclassifier.fit(X_train, Y_train)  
y_pred = svclassifier.predict(X_test)  
from sklearn.metrics import classification_report, confusion_matrix  
print(confusion_matrix(Y_test, y_pred))  
print(classification_report(Y_test, y_pred))  
#displayimage(imagelib, 2275, 2)



#def main():
#    imagelib = {}
#    imagepath = "../CUB_200_2011/images/"
#try:
#    imagelib is 
#    imagelib = importimages(imagepath)
#    displayimage(imagelib, 3, 5)
#  
#
#
#
#
#
#
#
#
#
#main()